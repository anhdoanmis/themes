<?php
/*
 * @package Incomda
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo home_url(); ?>">
    <div class="form-search">
        <input type="search" class="search-field" placeholder="<?php echo esc_html__('Search …', 'incomda'); ?>" value=""
               name="s">

        <button class="search-submit" type="submit">
            <i class="el el-search" aria-hidden="true"></i>
        </button>
    </div>
</form>

