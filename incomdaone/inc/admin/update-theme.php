<?php
function theme_pre_set_transient_update_theme ( $transient ) {
    if( empty( $transient->checked['incomdaone'] ) )
        return $transient;

    $result=file_get_contents("https://demo.incomda.com/sites/updates/update_incomda_one.json");
    // make sure that we received the data in the response is not empty
    if( empty( $result ) )
        return $transient;

    //check server version against current installed version
    if( $data = json_decode( $result ) ){
        if( version_compare( $transient->checked['incomdaone'], $data->new_version, '<' ) )
            $transient->response['incomdaone'] = (array) $data;
    }

    return $transient;

}
add_filter ( 'pre_set_site_transient_update_themes', 'theme_pre_set_transient_update_theme' );
