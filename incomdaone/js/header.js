jQuery(document).ready(function ($) {
    var offsettop = $('.site-branding-container').height();
    $('.blog #content.site-content').css('margin-top',offsettop+'px');
    $(window).scroll(function () {

        if ($(document).scrollTop() >= offsettop) {
            $(".site-branding-container").addClass("sticky");
        } else {
            $(".site-branding-container").removeClass("sticky");
        }
    });
});