<?php
/**
 * Incomda Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */

/**
 * Incomda Theme only works in WordPress 4.7 or later.
 */
if (version_compare($GLOBALS['wp_version'], '4.7', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
    return;
}

if (is_admin()) {
    require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';
    require_once get_template_directory() . '/inc/admin/plugins.php';

    /**
     * Demo Data
     */

    if (!function_exists('dgwork_import_files')) :
        function dgwork_import_files()
        {
            update_option('flush_elementor',1);
            return array(
                array(
                    'import_file_name' => 'Incomda Three Blue',
                    'local_import_file' => trailingslashit(get_template_directory()) . '/demo/content.xml',
                    'local_import_widget_file' => trailingslashit(get_template_directory()) . '/demo/widgets.wie',
                    'import_preview_image_url' => trailingslashit(get_template_directory()) . '/demo/screenshot.png',
                    'import_notice' => esc_html__('Please waiting for a few minutes, do not close the window or refresh the page until the data is imported.', 'dgwork'),
                ),
            );
        }
        add_filter('pt-ocdi/import_files', 'dgwork_import_files');
    endif;

    if (!function_exists('dgwork_after_import')) :
        function dgwork_after_import($selected_import)
        {

            if ('Incomda Three Blue' === $selected_import['import_file_name']) {
                //Set Menu
                $top_menu = get_term_by('name', 'Main Menu', 'nav_menu');

                set_theme_mod('nav_menu_locations', array(
                        'menu-1' => $top_menu->term_id,
                    )
                );

                $page = get_page_by_title('Home');
                if (isset($page->ID)) {
                    update_option('page_on_front', $page->ID);
                    update_option('show_on_front', 'page');
                }
            }
            update_option('flush_elementor',1);

        }

        add_action('pt-ocdi/after_import', 'dgwork_after_import');
    endif;


}
function clear_elementor_style(){
    if ( did_action( 'elementor/loaded' ) ) {
        update_option('flush_elementor', 0);
        \Elementor\Plugin::instance()->files_manager->clear_cache();
    }
}
if(get_option('flush_elementor') == 1)
    add_action('init','clear_elementor_style');

if (!function_exists('incomda_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function incomda_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Incomda Theme, use a find and replace
         * to change 'incomda' to the name of your theme in all the template files.
         */
        load_theme_textdomain('incomda', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1568, 9999);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(
            array(
                'menu-1' => esc_html__('Primary', 'incomda'),
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support(
            'custom-logo',
            array(
                'flex-width' => true,
                'flex-height' => true,
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        // Add support for Block Styles.
        add_theme_support('wp-block-styles');

        // Add support for full and wide align images.
        add_theme_support('align-wide');

        // Add support for editor styles.
        add_theme_support('editor-styles');


        // Add custom editor font sizes.
        add_theme_support(
            'editor-font-sizes',
            array(
                array(
                    'name' => esc_html__('Small', 'incomda'),
                    'shortName' => esc_html__('S', 'incomda'),
                    'size' => 19.5,
                    'slug' => 'small',
                ),
                array(
                    'name' => esc_html__('Normal', 'incomda'),
                    'shortName' => esc_html__('M', 'incomda'),
                    'size' => 22,
                    'slug' => 'normal',
                ),
                array(
                    'name' => esc_html__('Large', 'incomda'),
                    'shortName' => esc_html__('L', 'incomda'),
                    'size' => 36.5,
                    'slug' => 'large',
                ),
                array(
                    'name' => esc_html__('Huge', 'incomda'),
                    'shortName' => esc_html__('XL', 'incomda'),
                    'size' => 49.5,
                    'slug' => 'huge',
                ),
            )
        );

    }
endif;
add_action('after_setup_theme', 'incomda_setup');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function incomda_widgets_init()
{

    register_sidebar(
        array(
            'name' => esc_html__('Sidebar', 'incomda'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Add widgets here to appear in your sidebar.', 'incomda'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
    register_sidebar(
        array(
            'name' => esc_html__('Footer First Column', 'incomda'),
            'id' => 'footer-col-1',
            'description' => esc_html__('Add widgets here to appear in your footer.', 'incomda'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
    register_sidebar(
        array(
            'name' => esc_html__('Footer Second Column', 'incomda'),
            'id' => 'footer-col-2',
            'description' => esc_html__('Add widgets here to appear in your footer.', 'incomda'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
    register_sidebar(
        array(
            'name' => esc_html__('Footer Third Column', 'incomda'),
            'id' => 'footer-col-3',
            'description' => esc_html__('Add widgets here to appear in your footer.', 'incomda'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
    register_sidebar(
        array(
            'name' => esc_html__('Footer Fourth Column', 'incomda'),
            'id' => 'footer-col-4',
            'description' => esc_html__('Add widgets here to appear in your footer.', 'incomda'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
}

add_action('widgets_init', 'incomda_widgets_init');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function incomda_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('incomda_content_width', 640);
}

add_action('after_setup_theme', 'incomda_content_width', 0);

function theme_customize_register() {
    global $wp_customize;
    $wp_customize->remove_control('custom_logo');
}

add_action( 'customize_register', 'theme_customize_register', 11 );

/**
 * Enqueue scripts and styles.
 */
function incomda_scripts()
{

    wp_enqueue_style('bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(), '4.3.1');

    wp_enqueue_script('bootstrap-script', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '4.3.1', true);


    wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css', array(), '5.8.2' );

    wp_enqueue_script('header-script', get_template_directory_uri() . '/js/header.js', array(), filemtime(get_template_directory() . '/js/header.js'));

    wp_enqueue_style('theme-style', get_template_directory_uri() . '/style.css', array(), filemtime(get_template_directory() . '/style.css'));

    wp_enqueue_style('font-work-sans', 'https://fonts.googleapis.com/css?family=Work+Sans&display=swap', array(), '1.0');
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'incomda_scripts');

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function incomda_skip_link_focus_fix()
{
    // The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
    ?>
    <script>
        /(trident|msie)/i.test(navigator.userAgent) && document.getElementById && window.addEventListener && window.addEventListener("hashchange", function () {
            var t, e = location.hash.substring(1);
            /^[A-z0-9_-]+$/.test(e) && (t = document.getElementById(e)) && (/^(?:a|select|input|button|textarea)$/i.test(t.tagName) || (t.tabIndex = -1), t.focus())
        }, !1);
    </script>
    <?php
}

add_action('wp_print_footer_scripts', 'incomda_skip_link_focus_fix');

require get_template_directory() . '/classes/class-incomda-svg-icons.php';

/**
 * Custom Comment Walker template.
 */
require get_template_directory() . '/classes/class-incomda-walker-comment.php';

/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * SVG Icons related functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Theme options for the theme.
 */
require get_template_directory() . '/inc/theme-options.php';

/**
 * Theme updates for the theme.
 */
require get_template_directory() . '/inc/admin/update-theme.php';


