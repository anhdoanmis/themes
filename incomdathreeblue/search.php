<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */

get_header();
?>

    <section id="primary" class="content-area container">
        <main id="main" class="site-main">

            <?php if (have_posts()) : ?>

                <header class="page-header container">
                    <h1 class="page-title">
                        <?php _e('Search results for:', 'incomda'); ?>
                    </h1>
                    <div class="page-description"><?php echo get_search_query(); ?></div>
                </header><!-- .page-header -->

                <?php
                get_template_part('template-parts/blog/grid');

            // If no content, include the "No posts found" template.
            else :
                get_template_part('template-parts/content/content', 'none');

            endif;
            ?>
        </main><!-- #main -->
    </section><!-- #primary -->

<?php
get_footer();
