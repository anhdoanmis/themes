<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer ">
    <div class="footer-widgets">
        <div class="container">
            <?php get_template_part('template-parts/footer/footer', 'widgets'); ?>
        </div>
    </div>
    <div class="site-info col-md-12 float-left">
        <div class="container">
            <?php
            global $redux_options;
            $copyright = $redux_options['copyright-text'];
            if (!empty($copyright)) {
                echo '<span>' . esc_html($copyright) . '</span>';
            }
            echo '<a class="powered" href="https://webs.incomda.com/"  target="_blank">Powered by Incomda</a>';

            echo '<span class="social-network float-right border-left-0">';
            $layout = $redux_options['social-sorter']['enabled'];

            if ($layout): foreach ($layout as $key => $value) {
                if(isset($redux_options[$key . '-url-text']))
                    echo '<a class="item" href="' . esc_url($redux_options[$key . '-url-text']) . '"><i class="' . esc_html($redux_options[$key . '-icon-select']) . '"></i>'
                        . '</a>';
            }
            endif;
            echo '</span>';
            ?>


        </div>

    </div><!-- .site-info -->
</footer><!-- #colophon -->
<style>
    .site-top-bar .premium-button,footer .wpcf7-submit,.site-title,.blog-grid .blog-article .post-thumbnail,#cshero-menu-mobile .icon_menu .icon-bar{
        background-color: <?php echo esc_html($redux_options['header-nav-hover-color']);?>
    }

    .blog-grid .blog-article .article-container,.site-top-bar .right-items .premium-button{
        border-color: <?php echo esc_html($redux_options['header-nav-hover-color']);?>;
    }
</style>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
