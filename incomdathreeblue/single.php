<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */

get_header();
?>



    <section id="primary" class="content-area container">

        <main id="main" class="site-main col-md-9 col-sm-12 float-left">

            <?php

            /* Start the Loop */
            while (have_posts()) :
                the_post();
                get_template_part('template-parts/header/entry','header');
                get_template_part('template-parts/content/content', 'single');

                if (is_singular('attachment')) {
                    // Parent post navigation.
                    the_post_navigation(
                        array(
                            /* translators: %s: parent post link */
                            'prev_text' => sprintf(esc_html__('<span class="meta-nav">Published in</span><span class="post-title">%s</span>', 'incomda'), '%title'),
                        )
                    );
                } elseif (is_singular('post')) {
                    // Previous/next post navigation.
                    the_post_navigation(
                        array(
                            'next_text' => '<span class="meta-nav" aria-hidden="true">' . esc_html__('Next Post', 'incomda') . '</span> ' .
                                '<span class="screen-reader-text">' . esc_html__('Next post:', 'incomda') . '</span> <br/>' .
                                '<span class="post-title">%title</span>',
                            'prev_text' => '<span class="meta-nav" aria-hidden="true">' . esc_html__('Previous Post', 'incomda') . '</span> ' .
                                '<span class="screen-reader-text">' . esc_html__('Previous post:', 'incomda') . '</span> <br/>' .
                                '<span class="post-title">%title</span>',
                        )
                    );
                }

                // If comments are open or we have at least one comment, load up the comment template.
                if (comments_open() || get_comments_number()) {
                    comments_template();
                }

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
        <div class="sidebar-cols col-md-3 col-sm-12 float-left">
            <?php if (is_active_sidebar('sidebar-1')) : ?>

                <?php dynamic_sidebar('sidebar-1'); ?>

            <?php endif; ?>
        </div>
    </section><!-- #primary -->

<?php
get_footer();
