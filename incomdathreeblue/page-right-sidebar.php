<?php
/**
 * Template Name: Page With Right Sidebar
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */

get_header();
?>
    <section id="primary" class="content-area container">
        <main id="main" class="site-main col-md-9 col-sm-12 float-left">

            <?php


            while (have_posts()) :

                the_post();

                get_template_part('template-parts/content/content', 'page');

                // If comments are open or we have at least one comment, load up the comment template.
                if (comments_open() || get_comments_number()) :
                    comments_template();
                endif;

            endwhile; // End of the loop.


            ?>


        </main><!-- #main -->
        <div class="sidebar-cols col-md-3 col-sm-12 float-left">
            <?php if (is_active_sidebar('sidebar-1')) : ?>

                <?php dynamic_sidebar('sidebar-1'); ?>

            <?php endif; ?>
        </div>
    </section><!-- #primary -->

<?php
get_footer();
