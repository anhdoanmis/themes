<?php
add_action('tgmpa_register', 'icd_register_required_plugins');

/**
 * Register required plugins
 *
 * @since  1.0
 */
function icd_register_required_plugins()
{

    //$url = get_template_directory();

    $plugins = array(
        array(
            'name' => esc_html__('All-in-One WP Migration', 'incomda'),
            'slug' => 'all-in-one-wp-migration',
            'required' => true,
        ),
        array(
            'name' => esc_html__('Really Simple SSL', 'incomda'),
            'slug' => 'really-simple-ssl',
            'required' => true,
        ),
        array(
            'name' => esc_html__('Elementor Page Builder', 'incomda'),
            'slug' => 'elementor',
            'required' => true,
        ),
        array(
            'name' => esc_html__('One Click Demo Import', 'incomda'),
            'slug' => 'one-click-demo-import',
            'required' => true,
        ),
        array(
            'name' => esc_html__('Redux Framework', 'incomda'),
            'slug' => 'redux-framework',
            'required' => true,
        ),
        array(
            'name' => esc_html__('Contact Form 7', 'incomda'),
            'slug' => 'contact-form-7',
            'required' => true,
        ),

    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id' => 'incomda',
        // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',
        // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins',
        // Menu slug.
        'has_notices' => false,
        // Show admin notices or not.
        'dismissable' => false,
        // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '',
        // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => true,
        // Automatically activate plugins after installation or not.
        'message' => '',
        // Message to output right before the plugins table.
        'strings' => array(
            'page_title' => esc_html__('Install Required Plugins', 'incomda'),
            'menu_title' => esc_html__('Install Plugins', 'incomda'),
            'installing' => esc_html__('Installing Plugin: %s', 'incomda'),
            'updating' => esc_html__('Updating Plugin: %s', 'incomda'),
            'oops' => esc_html__('Something went wrong with the plugin API.', 'incomda'),
            'notice_can_install_required' => _n_noop(
                'This theme requires the following plugin: %1$s.',
                'This theme requires the following plugins: %1$s.',
                'incomda'
            ),
            'notice_can_install_recommended' => _n_noop(
                'This theme recommends the following plugin: %1$s.',
                'This theme recommends the following plugins: %1$s.',
                'incomda'
            ),
            'notice_ask_to_update' => _n_noop(
                'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
                'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
                'incomda'
            ),
            'notice_ask_to_update_maybe' => _n_noop(
                'There is an update available for: %1$s.',
                'There are updates available for the following plugins: %1$s.',
                'incomda'
            ),
            'notice_can_activate_required' => _n_noop(
                'The following required plugin is currently inactive: %1$s.',
                'The following required plugins are currently inactive: %1$s.',
                'incomda'
            ),
            'notice_can_activate_recommended' => _n_noop(
                'The following recommended plugin is currently inactive: %1$s.',
                'The following recommended plugins are currently inactive: %1$s.',
                'incomda'
            ),
            'install_link' => _n_noop(
                'Begin installing plugin',
                'Begin installing plugins',
                'incomda'
            ),
            'update_link' => _n_noop(
                'Begin updating plugin',
                'Begin updating plugins',
                'incomda'
            ),
            'activate_link' => _n_noop(
                'Begin activating plugin',
                'Begin activating plugins',
                'incomda'
            ),
            'return' => esc_html__('Return to Required Plugins Installer', 'incomda'),
            'plugin_activated' => esc_html__('Plugin activated successfully.', 'incomda'),
            'activated_successfully' => esc_html__('The following plugin was activated successfully:', 'incomda'),
            'plugin_already_active' => esc_html__('No action taken. Plugin %1$s was already active.', 'incomda'),
            'plugin_needs_higher_version' => esc_html__('Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'incomda'),
            'complete' => esc_html__('All plugins installed and activated successfully. %1$s', 'incomda'),
            'dismiss' => esc_html__('Dismiss this notice', 'incomda'),
            'notice_cannot_install_activate' => esc_html__('There are one or more required or recommended plugins to install, update or activate.', 'incomda'),
            'contact_admin' => esc_html__('Please contact the administrator of this site for help.', 'incomda'),
            'nag_type' => '',
            // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
        ),
    );

    tgmpa($plugins, $config);
}
