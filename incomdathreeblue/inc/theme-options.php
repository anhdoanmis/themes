<?php
/**
 * ReduxFramework Theme Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if (!class_exists('Redux')) {
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "redux_options";

// This line is only for altering the demo. Can be easily removed.
$opt_name = apply_filters('redux_options/opt_name', $opt_name);

/*
 *
 * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
 *
 */

$sampleHTML = '';
if (file_exists(dirname(__FILE__) . '/info-html.html')) {
    Redux_Functions::initWpFilesystem();

    global $wp_filesystem;

    $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
}

// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns = array();

if (is_dir($sample_patterns_path)) {

    if ($sample_patterns_dir = opendir($sample_patterns_path)) {
        $sample_patterns = array();

        while (($sample_patterns_file = readdir($sample_patterns_dir)) !== false) {

            if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                $name = explode('.', $sample_patterns_file);
                $name = str_replace('.' . end($name), '', $sample_patterns_file);
                $sample_patterns[] = array(
                    'alt' => $name,
                    'img' => $sample_patterns_url . $sample_patterns_file
                );
            }
        }
    }
}

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name' => $theme->get('Name'),
    // Name that appears at the top of your panel
    'display_version' => $theme->get('Version'),
    // Version that appears at the top of your panel
    'menu_type' => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => esc_html__('Theme Options', 'incomda'),
    'page_title' => esc_html__('Theme Options', 'incomda'),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key' => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => false,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon' => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => false,
    // Show the time the page took to load, etc
    'update_notice' => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority' => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => '',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn' => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    // HINTS
    'hints' => array(
        'icon' => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color' => 'lightgray',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'red',
            'shadow' => true,
            'rounded' => false,
            'style' => '',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'click mouseleave',
            ),
        ),
    )
);

// ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
$args['admin_bar_links'][] = array(
    'id' => 'redux-docs',
    'href' => 'http://docs.reduxframework.com/',
    'title' => esc_html__('Documentation', 'incomda'),
);

$args['admin_bar_links'][] = array(
    //'id'    => 'redux-support',
    'href' => 'https://github.com/ReduxFramework/redux-framework/issues',
    'title' => esc_html__('Support', 'incomda'),
);

$args['admin_bar_links'][] = array(
    'id' => 'redux-extensions',
    'href' => 'reduxframework.com/extensions',
    'title' => esc_html__('Extensions', 'incomda'),
);

// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
$args['share_icons'][] = array(
    'url' => 'https://github.com/ReduxFramework/ReduxFramework',
    'title' => 'Visit us on GitHub',
    'icon' => 'el el-github'
    //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
);
$args['share_icons'][] = array(
    'url' => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
    'title' => 'Like us on Facebook',
    'icon' => 'el el-facebook'
);
$args['share_icons'][] = array(
    'url' => 'http://twitter.com/reduxframework',
    'title' => 'Follow us on Twitter',
    'icon' => 'el el-twitter'
);
$args['share_icons'][] = array(
    'url' => 'http://www.linkedin.com/company/redux-framework',
    'title' => 'Find us on LinkedIn',
    'icon' => 'el el-linkedin'
);


Redux::setArgs($opt_name, $args);

/*
 * ---> END ARGUMENTS
 */


/*
 * ---> START HELP TABS
 */

$tabs = array(
    array(
        'id' => 'redux-help-tab-1',
        'title' => esc_html__('Theme Information 1', 'incomda'),
        'content' => esc_html__('<p>This is the tab content, HTML is allowed.</p>', 'incomda')
    ),
    array(
        'id' => 'redux-help-tab-2',
        'title' => esc_html__('Theme Information 2', 'incomda'),
        'content' => esc_html__('<p>This is the tab content, HTML is allowed.</p>', 'incomda')
    )
);
Redux::setHelpTab($opt_name, $tabs);

// Set the help sidebar
$content = esc_html__('<p>This is the sidebar content, HTML is allowed.</p>', 'incomda');
Redux::setHelpSidebar($opt_name, $content);


/*
 * <--- END HELP TABS
 */


/*
 *
 * ---> START SECTIONS
 *
 */

/*

    As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


 */

// <--- START SECTIONS

Redux::setSection($opt_name, array(
    'title' => esc_html__('Header', 'incomda'),
    'id' => 'header',
    'icon' => 'el el-th-large',
    'fields' => array(
        array(
            'id' => 'top-bar-section-start',
            'type' => 'section',
            'title' => esc_html__('Top Bar', 'incomda'),
            'subtitle' => esc_html__('The Bar\'s on the top of page.', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'top-bar-show-switch',
            'type' => 'switch',
            'title' => esc_html__('Show Top Bar', 'incomda'),
            'subtitle' => esc_html__('', 'incomda'),
            'default' => true,
        ),
        array(
            'id' => 'phone-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Phone Icon', 'incomda'),
            'default' => 'el el-phone',
        ),
        array(
            'id' => 'phone-text',
            'type' => 'text',
            'title' => esc_html__('Phone Number', 'incomda'),
            'default' => '+028 62895123',
        ),
        array(
            'id' => 'email-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Email Icon', 'incomda'),
            'default' => 'el el-envelope',
        ),
        array(
            'id' => 'email-text',
            'type' => 'text',
            'title' => esc_html__('Email Number', 'incomda'),
            'default' => 'info@incomda.com',
        ),
        array(
            'id' => 'right-button-section-start',
            'type' => 'section',
            'title' => esc_html__('Right Button', 'incomda'),
            'subtitle' => esc_html__('The button\'s on the right side.', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'right-button-switch',
            'type' => 'switch',
            'title' => esc_html__('Show Right Button', 'incomda'),
            'subtitle' => esc_html__('', 'incomda'),
            'default' => true,
        ),
        array(
            'id' => 'right-button-tilte-text',
            'type' => 'text',
            'title' => esc_html__('Right Button Text', 'incomda'),
            'default' => 'Get A Quote',
        ),
        array(
            'id' => 'right-button-link-text',
            'type' => 'text',
            'title' => esc_html__('Right Button Url', 'incomda'),
            'default' => '#',
        ),
        array(
            'id' => 'right-button-section-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'top-bar-section-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'sticky-switch',
            'type' => 'switch',
            'title' => esc_html__('Sticky Menu', 'incomda'),
            'subtitle' => esc_html__('', 'incomda'),
            'default' => true,
        ),
        array(
            'id' => 'logo-media',
            'type' => 'media',
            'url' => true,
            'title' => esc_html__('Header Logo', 'incomda'),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc' => esc_html__('Basic media uploader with disabled URL input field.', 'incomda'),
            'subtitle' => esc_html__('Upload any media using the WordPress native uploader', 'incomda'),
            //'default'  => array( 'url' => 'https://s.wordpress.org/style/images/codeispoetry.png' ),
            //'hint'      => array(
            //    'title'     => 'Hint Title',
            //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
            //)
        ),
        array(
            'id'       => 'opt-typography-nav',
            'type'     => 'typography',
            'title'    => __( 'Menu Font', 'incomda' ),
            'subtitle' => __( 'Specify the menu font properties.', 'incomda' ),
            'color'    => false,
            'google'   => true,
            'output' => array('.site-branding-container .site-branding ul.main-menu a','.main-navigation-mobile .main-menu a '),
            'default'  => array(
                'font-size'   => '17px',
                'font-family' => 'Roboto',
                'font-weight' => '700',
            ),
        ),
        array(
            'id' => 'header-background-color-rgba',
            'type' => 'color_rgba',
            'title' => esc_html__('Header Sticky Background Color', 'incomda'),
            'subtitle' => esc_html__('The RGBA color.', 'incomda'),
            'default' => array(
                'color' => '#595959',
                'alpha' => '.17'
            ),
            'output' => array('.site-branding-container.sticky','.site-branding-container.bg'),
            'mode' => 'background',
            'validate' => 'colorrgba',
        ),
        array(
            'id' => 'header-nav-color',
            'type' => 'color',
            'title' => esc_html__('Primary Font Color', 'incomda'),
            'subtitle' => esc_html__('Pick a background color for the Primary\'s Font (default: #ffffff).', 'incomda'),
            'default' => '#ffffff',
            'validate' => 'color',
            'output' => array('.site-branding-container .site-branding ul.main-menu > li > a'),
        ),
        array(
            'id' => 'header-nav-hover-color',
            'type' => 'color',
            'title' => esc_html__('Primary Hover Color', 'incomda'),
            'subtitle' => esc_html__('Pick a background color for the Primary\'s Font Hover (default: #d7af50).', 'incomda'),
            'default' => '#d7af50',
            'validate' => 'color',
            'output' => array('.site-branding-container .site-branding ul.main-menu > li.current-menu-item > a', '.site-branding-container .site-branding ul.main-menu > li:hover > a'
            ,'.site-top-bar .left-items > span i','.widget-area i','footer .site-info span i:hover','a:hover','.widget a','#cshero-menu-mobile .text','a'),
        ),


    )
));
Redux::setSection($opt_name, array(
    'title' => esc_html__('Footer', 'incomda'),
    'id' => 'footer',
    'icon' => 'el el-hand-down',
    'fields' => array(
        array(
            'id' => 'copyright-text',
            'type' => 'text',
            'title' => esc_html__('Copyright Text', 'incomda'),
            'default' => 'Copyright © 2008 - 2019 Incomda Corporation',
        ),
    )
));
Redux::setSection($opt_name, array(
        'title' => esc_html__('Social Network', 'incomda'),
        'id' => 'social-network',
        'icon' => 'el el-address-book',
        'fields' => array(
        array(
            'id' => 'social-sorter',
            'type' => 'sorter',
            'title' => 'Social Network',
            'desc' => 'Organize how you want the social network to appear on the footer',
            'compiler' => 'true',
            'options' => array(
                'enabled' => array(
                    'facebook' => 'Facebook',
                    'twitter' => 'Twitter',
                    'google-plus' => 'G +',
                    'instagram' => 'Instagram',
                    'dribbble' => 'Dribbble',
                    'youtube' => 'Youtube',
                    'pinterest' => 'Pinterest',
                ),
                'disabled' => array(),
            ),
        ),
        array(
            'id' => 'facebook-start',
            'type' => 'section',
            'title' => esc_html__('Facebook', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'facebook-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Icon', 'incomda'),
            'default' => 'el el-facebook',
        ),
        array(
            'id' => 'facebook-url-text',
            'type' => 'text',
            'title' => esc_html__('URL', 'incomda'),
            'default' => 'https://facebook.com',
        ),
        array(
            'id' => 'facebook-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),


        array(
            'id' => 'twitter-start',
            'type' => 'section',
            'title' => esc_html__('Twitter', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'twitter-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Icon', 'incomda'),
            'default' => 'el el-twitter',
        ),
        array(
            'id' => 'twitter-url-text',
            'type' => 'text',
            'title' => esc_html__('URL', 'incomda'),
            'default' => 'https://twitter.com',
        ),
        array(
            'id' => 'twitter-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),


        array(
            'id' => 'google-plus-start',
            'type' => 'section',
            'title' => esc_html__('G +', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'google-plus-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Icon', 'incomda'),
            'default' => 'el el-googleplus',
        ),
        array(
            'id' => 'google-plus-url-text',
            'type' => 'text',
            'title' => esc_html__('URL', 'incomda'),
            'default' => 'https://plus.google.com',
        ),
        array(
            'id' => 'google-plus-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),

        array(
            'id' => 'instagram-start',
            'type' => 'section',
            'title' => esc_html__('Instagram', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'instagram-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Icon', 'incomda'),
            'default' => 'el el-instagram',
        ),
        array(
            'id' => 'instagram-url-text',
            'type' => 'text',
            'title' => esc_html__('URL', 'incomda'),
            'default' => 'https://instagram.com',
        ),
        array(
            'id' => 'instagram-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),

        array(
            'id' => 'dribbble-start',
            'type' => 'section',
            'title' => esc_html__('Dribbble', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'dribbble-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Icon', 'incomda'),
            'default' => 'el el-dribbble',
        ),
        array(
            'id' => 'dribbble-url-text',
            'type' => 'text',
            'title' => esc_html__('URL', 'incomda'),
            'default' => 'https://dribbble.com',
        ),
        array(
            'id' => 'dribbble-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),

        array(
            'id' => 'youtube-start',
            'type' => 'section',
            'title' => esc_html__('Youtube', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'youtube-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Icon', 'incomda'),
            'default' => 'el el-youtube',
        ),
        array(
            'id' => 'youtube-url-text',
            'type' => 'text',
            'title' => esc_html__('URL', 'incomda'),
            'default' => 'https://youtube.com',
        ),
        array(
            'id' => 'youtube-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),

        array(
            'id' => 'pinterest-start',
            'type' => 'section',
            'title' => esc_html__('Pinterest', 'incomda'),
            'indent' => true, // Indent all options below until the next 'section' option is set.
        ),
        array(
            'id' => 'pinterest-icon-select',
            'type' => 'select',
            'data' => 'elusive-icons',
            'title' => esc_html__('Icon', 'incomda'),
            'default' => 'el el-pinterest',
        ),
        array(
            'id' => 'pinterest-url-text',
            'type' => 'text',
            'title' => esc_html__('URL', 'incomda'),
            'default' => 'https://pinterest.com',
        ),
        array(
            'id' => 'pinterest-end',
            'type' => 'section',
            'indent' => false, // Indent all options below until the next 'section' option is set.
        ),


    )
));
Redux::setSection( $opt_name, array(
    'title'  => __( 'Typography', 'incomda' ),
    'id'     => 'typography',
    'icon'   => 'el el-font',
    'fields' => array(
        array(
            'id'       => 'opt-typography-body',
            'type'     => 'typography',
            'title'    => __( 'Body Font', 'incomda' ),
            'subtitle' => __( 'Specify the body font properties.', 'incomda' ),
            'google'   => true,
            'output' => array('body'),
            'default'  => array(
                'color'       => '#000',
                'font-size'   => '16px',
                'font-family' => 'Roboto',
                'font-weight' => 'Normal',
            ),
        ),
    )
) );
/*
 * <--- END SECTIONS
 */


/*
 *
 * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
 *
 */

/*
*
* --> Action hook examples
*
*/

// If Redux is running as a plugin, this will remove the demo notice and links
//add_action( 'redux/loaded', 'remove_demo' );

// Function to test the compiler hook and demo CSS output.
// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
//add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

// Change the arguments after they've been declared, but before the panel is created
//add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

// Change the default value of a field after it's been set, but before it's been useds
//add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

// Dynamically add a section. Can be also used to modify sections/fields
//add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

/**
 * This is a test function that will let you see when the compiler hook occurs.
 * It only runs if a field    set with compiler=>true is changed.
 * */
if (!function_exists('compiler_action')) {
    function compiler_action($options, $css, $changed_values)
    {
        echo '<h1>The compiler hook has run!</h1>';
        echo "<pre>";
        print_r($changed_values); // Values that have changed since the last save
        echo "</pre>";
        //print_r($options); //Option values
        //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
    }
}

/**
 * Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')) {
    function redux_validate_callback_function($field, $value, $existing_value)
    {
        $error = false;
        $warning = false;

        //do your validation
        if ($value == 1) {
            $error = true;
            $value = $existing_value;
        } elseif ($value == 2) {
            $warning = true;
            $value = $existing_value;
        }

        $return['value'] = $value;

        if ($error == true) {
            $field['msg'] = 'your custom error message';
            $return['error'] = $field;
        }

        if ($warning == true) {
            $field['msg'] = 'your custom warning message';
            $return['warning'] = $field;
        }

        return $return;
    }
}

/**
 * Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')) {
    function redux_my_custom_field($field, $value)
    {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
}

/**
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 * */
if (!function_exists('dynamic_section')) {
    function dynamic_section($sections)
    {
        //$sections = array();
        $sections[] = array(
            'title' => esc_html__('Section via hook', 'incomda'),
            'desc' => esc_html__('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'incomda'),
            'icon' => 'el el-paper-clip',
            // Leave this as a blank section, no options just some intro text set above.
            'fields' => array()
        );

        return $sections;
    }
}

/**
 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
 * */
if (!function_exists('change_arguments')) {
    function change_arguments($args)
    {
        //$args['dev_mode'] = true;

        return $args;
    }
}

/**
 * Filter hook for filtering the default value of any given field. Very useful in development mode.
 * */
if (!function_exists('change_defaults')) {
    function change_defaults($defaults)
    {
        $defaults['str_replace'] = 'Testing filter hook!';

        return $defaults;
    }
}

/**
 * Removes the demo link and the notice of integrated demo from the redux-framework plugin
 */
if (!function_exists('remove_demo')) {
    function remove_demo()
    {
        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if (class_exists('ReduxFrameworkPlugin')) {
            remove_filter('plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2);

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
        }
    }
}

