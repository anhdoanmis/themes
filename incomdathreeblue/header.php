<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="profile" href="https://gmpg.org/xfn/11"/>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

    <header id="masthead" class="<?php echo is_singular() && incomda_can_show_post_thumbnail() ? 'site-header featured-image' : 'site-header'; ?>">
        <div class="site-top-bar-container">
            <?php get_template_part( 'template-parts/header/top', 'bar' ); ?>
        </div>
        <?php
        global $redux_options;
        $is_sticky = $redux_options['sticky-switch'];
        ?>
        <div class="nav-container">
            <div class="site-branding-container navbar<?php if($is_sticky):?>-sticky<?php endif;?> <?php if ((!is_front_page() && !is_home())||'post' == get_post_type()) echo 'bg';?>" >
                <?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
            </div><!-- .site-branding-container -->
            <div class="site-branding-container" style="opacity: 0;z-index:-1;">
                <?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
            </div>
            <?php if ( (!is_front_page() && !is_home())||'post' == get_post_type()):?>
                <div class="site-branding-container-hidden">
                    <?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
                </div><!-- .site-branding-container -->
            <?php endif;?>
        </div>
        <div id="searchModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form role="search" method="get" action="<?php echo home_url();?>">
                        <input type="text" value="" name="s" placeholder="<?php echo __('SEARCH HERE...','incomda');?>" />
                    </form>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->

    <div id="content" class="site-content">

