jQuery(document).ready(function ($) {
    var offsettop = $('.site-top-bar-container').height();
    var offsetwp = 0;
    if($('#wpadminbar').length) {
        offsetwp = $('#wpadminbar').height();
    }
    var top_height = offsetwp + offsettop;
    var offsetwppx = offsetwp + 'px';
    if(top_height==0)top_height=1;
    $(window).scroll(function () {
        if ($(document).scrollTop() >= top_height) {
            //$(".site-branding-container-hidden").css('display','block');
            $(".site-branding-container.navbar-sticky").css('top',offsetwppx);
            $(".site-branding-container.navbar-sticky").css('position','fixed');
            $(".site-branding-container.navbar-sticky").addClass("sticky");
        } else {
            //$(".site-branding-container-hidden").css('display','none');
            $(".site-branding-container.narbar-sticky").css('top',0);
            $(".site-branding-container.navbar-sticky").css('position','absolute');
            $(".site-branding-container.navbar-sticky").removeClass("sticky");
        }
    });
});