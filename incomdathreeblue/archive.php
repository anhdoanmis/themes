<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */

get_header();
?>

    <section id="primary" class="content-area container">
        <main id="main" class="site-main col-md-9 col-sm-12 float-left">

            <?php

            get_template_part('template-parts/blog/grid');

            // If comments are open or we have at least one comment, load up the comment template.


            ?>


        </main><!-- #main -->
        <div class="sidebar-cols col-md-3 col-sm-12 float-left">
            <?php if (is_active_sidebar('sidebar-1')) : ?>

                <?php dynamic_sidebar('sidebar-1'); ?>

            <?php endif; ?>
        </div>
    </section><!-- #primary -->

<?php
get_footer();
