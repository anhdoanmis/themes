<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incomda
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('container'); ?>>

    <?php incomda_the_post_thumbnail(); ?>

    <div class="article-container">
        <header class="entry-header">
            <?php
            if (is_singular()) :
                the_title('<h1 class="entry-title">', '</h1>');
            else :
                the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
            endif;

            if ('post' === get_post_type()) : ?>
                <div class="entry-meta">
                    <?php incomda_posted_on(); ?>
                </div><!-- .entry-meta -->
            <?php
            endif; ?>

        </header><!-- .entry-header -->

        <div class="entry-content">
            <?php the_excerpt(); ?>
            <div class="parent-more-link"><a href="<?php echo esc_url(get_permalink()); ?>"
                                             class="more-link"><?php echo esc_html__('Read more', 'incomda'); ?></a></div>
        </div><!-- .entry-content -->

        <footer class="entry-footer">
            <?php //incomda_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    </div>

</article><!-- #post-<?php the_ID(); ?> -->
