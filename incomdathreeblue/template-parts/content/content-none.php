<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */

?>

<section class="no-results not-found">
    <header class="page-header  container">
        <h1 class="page-title"><?php _e('Nothing Found', 'incomda'); ?></h1>
    </header><!-- .page-header -->

    <div class="page-content">
        <?php
        if (is_home() && current_user_can('publish_posts')) :

            printf(
                '<p>' . wp_kses(
                /* translators: 1: link to WP admin new post page. */
                    esc_html__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'incomda'),
                    array(
                        'a' => array(
                            'href' => array(),
                        ),
                    )
                ) . '</p>',
                esc_url(admin_url('post-new.php'))
            );

        elseif (is_search()) :
            ?>

            <p><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'incomda'); ?></p>
            <?php
            get_search_form();

        else :
            ?>

            <p><?php _e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'incomda'); ?></p>
            <?php
            get_search_form();

        endif;
        ?>
    </div><!-- .page-content -->
</section><!-- .no-results -->
