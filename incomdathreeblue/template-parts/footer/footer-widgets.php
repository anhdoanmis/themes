<?php
/**
 * Displays the footer widget area
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */

if (is_active_sidebar('footer-col-1')) : ?>

    <aside class="widget-area col-md-3 col-sm-12 col-12 float-left" role="complementary"
           aria-label="<?php esc_attr_e('Footer', 'incomda'); ?>">

        <div class="widget-column footer-widget-1">
            <?php dynamic_sidebar('footer-col-1'); ?>
        </div>

    </aside><!-- .widget-area -->

<?php endif;
if (is_active_sidebar('footer-col-2')) : ?>

    <aside class="widget-area col-md-3 col-sm-12 col-12 float-left" role="complementary"
           aria-label="<?php esc_attr_e('Footer', 'incomda'); ?>">

        <div class="widget-column footer-widget-2">
            <?php dynamic_sidebar('footer-col-2'); ?>
        </div>

    </aside><!-- .widget-area -->

<?php endif;
if (is_active_sidebar('footer-col-3')) : ?>

    <aside class="widget-area col-md-3 col-sm-12 col-12 float-left" role="complementary"
           aria-label="<?php esc_attr_e('Footer', 'incomda'); ?>">

        <div class="widget-column footer-widget-3">
            <?php dynamic_sidebar('footer-col-3'); ?>
        </div>

    </aside><!-- .widget-area -->

<?php endif; if (is_active_sidebar('footer-col-4')) : ?>

    <aside class="widget-area col-md-3 col-sm-12 col-12 float-left" role="complementary"
           aria-label="<?php esc_attr_e('Footer', 'incomda'); ?>">

        <div class="widget-column footer-widget-4">
            <?php dynamic_sidebar('footer-col-4'); ?>
        </div>

    </aside><!-- .widget-area -->

<?php endif; ?>


