<?php
/**
 * Displays the post header
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */
?>
<?php if(!is_page()):?>
<div class="site-title">
    <div class="container">
<?php the_title( '<h1 class="entry-title container">', '</h1>' ); ?>
    </div>
</div>
<?php endif;?>


