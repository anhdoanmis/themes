<?php
/**
 * Displays header top bar
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */
?>
<?php
global $redux_options;
$show_top_bar = $redux_options['top-bar-show-switch'];
if($show_top_bar):
    $show_button = $redux_options['right-button-switch'];
?>
<div class="site-top-bar container ">
    <div class="col-md-<?php echo $show_button? '8':'12';?> col-sm-12 float-left">
        <div class="left-items">
            <span>
                <i class="<?php  echo esc_html($redux_options['phone-icon-select']);?>"></i> <?php  echo esc_html($redux_options['phone-text']);?>
            </span>
            <span class="hidden-sm">
                <i class="<?php  echo esc_html($redux_options['email-icon-select']);?>"></i> <?php  echo esc_html($redux_options['email-text']);?>
            </span>
        </div>
    </div>
    <?php
        if($show_button):
    ?>
    <div class="col-md-4 col-sm-12  float-left  ">
        <div class="right-items">
            <a class="premium-button float-right" href="<?php echo esc_url($redux_options['right-button-link-text']);?>"><?php echo esc_html($redux_options['right-button-tilte-text']);?></a>
        </div>
    </div>
    <?php
    endif;
    ?>
</div>
<?php endif;?>
