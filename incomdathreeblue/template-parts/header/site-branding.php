<?php
/**
 * Displays header site branding
 *
 * @package Incomda
 * @subpackage Incomda_Theme
 * @since 1.0.0
 */
?>
<div class="site-branding container">
    <div class="site-logo col-md-3 col-sm-6 col-6 float-left">
        <?php
        global $redux_options;
        $logo_img = !empty($redux_options['logo-media']['id']) ?
            '<img height="30" src="' . esc_url($redux_options['logo-media']['url']) . '" class="custom-logo" alt="' . esc_html($redux_options['logo-media']['alt']) . '" title="' . esc_html($redux_options['logo-media']['title']) . '">' :
            '<img height="30" src="' . esc_url(get_stylesheet_directory_uri()) . '/img/logo-inc.png" class="custom-logo" alt="Incomda Theme">';
        ?>
        <a href="<?php echo esc_url(home_url()); ?>" class="custom-logo-link" rel="home">
            <?php echo $logo_img; ?>
        </a>
    </div>
    <?php if (has_nav_menu('menu-1')) : ?>
        <div id="cshero-menu-mobile" class="hidden-md col-sm-6 col-6 float-left">
            <button data-target="#site-navigation-mobile" data-toggle="collapse" class="navbar-toggle btn-navbar"
                    type="button" aria-expanded="false">
                <span class="text"><?php echo esc_html__('MENU', 'incomda'); ?></span>
                <span aria-hidden="true" class="icon_menu main-menu-icon">
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </span>
            </button>
        </div>
        <nav id="site-navigation" class="main-navigation col-md-9 hidden-sm  float-left"
             aria-label="<?php esc_attr_e('Top Menu', 'incomda'); ?>">
            <?php

            wp_nav_menu(
                array(
                    'theme_location' => 'menu-1',
                    'menu_class' => 'main-menu',
                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s<li class="menu-item button-search"><a href="javascrip:void(0);" data-toggle="modal" data-target="#searchModal"><i class="el el-search"></i></a></li></ul>',
                    'link_after' => '<span class="sub-arrow"><i class="fa fa-caret-down"></i></span>',
                )
            );
            ?>
        </nav><!-- #site-navigation -->
    <?php endif; ?>
</div><!-- .site-branding -->
<nav id="site-navigation-mobile" class="main-navigation-mobile collapse" aria-expanded="false"
     aria-label="<?php esc_attr_e('Top Menu', 'incomda'); ?>">

    <?php
    wp_nav_menu(
        array(
            'theme_location' => 'menu-1',
            'menu_class' => 'main-menu',
            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s<li class="menu-item button-search">
                                        <form role="search" method="get" action="'.home_url().'"> <input type="text" value="" name="s" placeholder="'.__('SEARCH HERE...','incomda').'"> 
                                            <input type="submit" value="Search"> 
                                            <span class="icon-search">
                                                <span class="icon_search main-menu-icon" aria-hidden="true"></span>
                                            </span> 
                                        </form>
                                    </li></ul>',
            'after' => '<span class="cs-menu-toggle"><span class="hidden"></span></span>',
        )
    );
    ?>
</nav><!-- #site-navigation -->
