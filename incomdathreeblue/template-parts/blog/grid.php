<?php
/**
 * Custom CSS for blog grid.
 *
 * @package Incomda
 */

?>

<div class="blog-grid container">
    <main class="row">
        <?php
        while (have_posts()) :

            the_post();

            echo '<div class="blog-article col-md-6">';
            get_template_part('template-parts/content/content', 'grid');
            echo '</div>';

        endwhile;
        ?>
    </main>

    <?php
    the_posts_pagination(
        array(
            'prev_text' => esc_html__('<i class="fa fa-angle-double-left" aria-hidden="true"></i>', 'incomda'),
            'next_text' => esc_html__('<i class="fa fa-angle-double-right" aria-hidden="true"></i>', 'incomda'),
        )
    );
    ?>
</div>
